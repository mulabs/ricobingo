import image1 from './assets/images/rico1.jpg';
import image2 from './assets/images/rico2.jpg';
import image3 from './assets/images/rico3.jpg';
import image4 from './assets/images/rico4.jpg';
import image5 from './assets/images/rico5.jpg';
import image6 from './assets/images/rico6.jpg';
import image7 from './assets/images/rico7.jpg';
import image8 from './assets/images/rico8.jpg';
import image9 from './assets/images/rico9.jpg';
import image10 from './assets/images/rico10.jpg';
import image11 from './assets/images/rico11.jpg';
import image12 from './assets/images/rico12.jpg';
import image13 from './assets/images/rico13.jpg';
import image14 from './assets/images/rico14.jpg';
import image15 from './assets/images/rico15.jpg';
import image16 from './assets/images/rico16.jpg';
import image17 from './assets/images/rico17.jpg';
import image18 from './assets/images/rico18.jpg';
import image19 from './assets/images/rico19.jpg';


const cards = [
    {id: 'rico1', fname: 'Donald Trump', src:image1, flip:false, biog: '45th President'},
    {id: 'rico2', fname: 'Rudy Giuliani', src:image2, flip:false, biog: 'Former mayor of New York City, personal lawyer for Mr Trump'},
    {id: 'rico3', fname: 'John Eastman', src: image3, flip:false, biog: 'Former lawyer for Mr Trump, represented the former president in a lawsuit trying to overturn election results'},
    {id: 'rico4', fname: 'Mark Meadows', src:image4, flip:false, biog: 'Former Chief of Staff'},
    {id: 'rico5', fname: 'Kenneth Cheesbro', src: image5 ,flip:true, biog: 'Wisconsin appellate lawyer, accused of helping to devise a plan to submit fake slates of electors.'},
    {id: 'rico6', fname: 'Jeffrey Clark', src: image6, flip:false, biog: 'Former DoJ official,accused of trying to persuade the then acting attorney-general to write to the Georgian authorities claiming voting irregularities'},
    {id: 'rico7', fname: 'Jenna Ellis', src: image7, flip:true, biog: 'Former Trump attorney, alleged to have been involved in trying to get false electors appointed in four states'},
    {id: 'rico8', fname: 'Ray Smith', src: image8, flip:false, biog: 'member of the Trump legal team who filed one of the campaign\'s election challenges in a state court'},
    {id: 'rico9', fname: 'Robert Cheeley', src: image9, flip:false, biog: 'Georgia-based lawyer, accused of making unfounded accusations of election worker interference'},
    {id: 'rico10', fname: 'Mike Roman', src: image10, flip:false, biog: 'Senior member of the Trump election campaign, said to have played a role in organising the fake electors\' plot'},
    {id: 'rico11', fname: 'David Shafer', src: image11, flip:false, biog: 'Then chairman of the Georgian Republican Party, charged with mailing a fake certificate of Trump electors to a federal courthouse, and making false statements to investigators.'},
    {id: 'rico12', fname: 'Shawn Still', src: image12, flip:false, biog: 'Then finance chair of the state Republican Party and now a Georgian state senator, he was another of the fake electors.'},
    {id: 'rico13', fname: 'Stephen Lee', src: image13, flip:false, biog: 'Lutheran pastor from Illinois accused of efforts to intimidate election workers in Atlanta'},
    {id: 'rico14', fname: 'Harrison Floyd', src: image14, flip:false, biog: 'Former head of a group called Black Voices for Trump, accused of intimidation of Atlanta election workers'},
    {id: 'rico15', fname: 'Trevian Kutti', src: image15, flip:false, biog: 'Former publicist for R Kelly and Ye, accused of involvement in a plot to pressure a Fulton County election worker to falsely admit committing fraud'},
    {id: 'rico16', fname: 'Sidney Powell', src: image16, flip:true, biog: 'Former Trump attorney closely tied to Mr Trump\'s attempts to challenge the 2020 election results. She is also alleged to have been involved with an illegal breach of election data in Coffee County, Georgia,'},
    {id: 'rico17', fname: 'Cathy Latham', src: image17, flip:false, biog: 'Former Republican Party chair in Coffee County and one of the 16 fake Trump electors for the state'},
    {id: 'rico18', fname: 'Scott Hall', src: image18, flip:true, biog: 'Bail bondsman and Trump supporter involved in allegedly trying to gain access to sensitive election equipment in Coffee County'},
    {id: 'rico19', fname: 'Misty Hampton', src: image19, flip:false, biog: 'Former election supervisor of Coffee County, who is alleged to have helped Trump supporters gain access to the county\'s voting equipment.'},
];
export default cards;