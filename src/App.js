import './App.scss';
import Gallery from './Gallery';
import geseal from './assets/ico/ge_stamp.jpg';

function App() {
  return (
    <div className="App">
      <div className='header'>
        <img src={geseal} alt=''/>
        <h1>Rico Bingo</h1>
        <img src={geseal} alt=''/>
      </div>
      <Gallery/>
    </div>
  );
}

export default App;
