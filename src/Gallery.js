import React from "react";
import cards from './carddata';
import flipico from './assets/ico/unorevico.png';

const CDNURL = "https://ldjwhogbdhlegomntajh.supabase.co/storage/v1/object/sign/grico/";

function Card (cards){
    return (
    <div className="card-container">
        <input type="checkbox" id={cards.id}/>
        <label className="card" htmlFor={cards.id}>
            <div className="front">
                <h3 style={{textAlign:'center', fontWeight:'bold'}}>{cards.title}</h3>
                <img src={cards.src} key={cards.id} alt={cards.id}/>
                <img className={cards.flip ? 'flipicon' : 'biglie'} src={flipico} alt=''/>
            </div>
            <div className="back">
                <img src={cards.src} key={cards.id} alt={cards.id}/>
                <h4>{cards.biog}</h4>
            </div>
        </label>
    </div>
    );
}

const Gallery =()=>{

    return (
    <div className="gallery">
        {cards.map((card) => {
            return <Card key={card.id} id={card.id} title={card.fname} src={card.src} biog={card.biog} flip={card.flip}/>;
        })
        }
    </div>
    );
}
export default Gallery;