import { useEffect, useState } from "react";
import { createClient } from "@supabase/supabase-js";

const supabase = createClient("https://ldjwhogbdhlegomntajh.supabase.co", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Imxkandob2diZGhsZWdvbW50YWpoIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDEzNTAyNDcsImV4cCI6MjAxNjkyNjI0N30.ZwMcIzn7oY0FbSZuFqsGhtu9SZysmIUARL_kJInSL2U");
const CDNURL = "https://ldjwhogbdhlegomntajh.supabase.co/storage/v1/object/sign/grico/";
function SupApp() {
  const [dancecard, setDancecard] = useState([]);
  const [images, setImages] = useState([]);

  useEffect(() => {
    getDancecard();
    getImages();
  }, []);   

  async function getDancecard() {
    const { data } = await supabase.from("dancecard").select();
    setDancecard(data);
  }
  async function getImages() {
    const {data, error} = await supabase
    .storage
    .from('grico')
    .list("/", {});
    if(data !== null){
        setImages(data)
    }else {
        alert('Error loading mugshots');
    }
  }

  return (
    <ul>
      {dancecard.map((card) => (
        <li key={card.id}>{card.fname}</li>
      ))}
    </ul>
  );
}

export default SupApp;